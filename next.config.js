const withPWA = require("next-pwa");

module.exports = withPWA({
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: "empty"
    };

    return config;
  },
  pwa: {
    disable: false,
    dest: "public"
  },
  experimental: {
    publicDirectory: true
  }
});
