import { useCallback, useContext, useEffect, useState, useRef } from "react";
import { todoContext } from "../contexts/todoContext";
import { userContext } from "../contexts/userContext";
import TodoList from "../types/TodoList";
import TodoItem from "../types/TodoItem";
import TodoListJSX from "../components/TodoList";
import NewTodoList from "../components/NewTodoList";
import Pusher from "pusher-js";
import pusherConfig from "../config/pusherconfig";
import { NextPageContext } from "next";
import { useTodoSubscription } from "../utils/useTodoSubscriptions";
import uuid from "uuid/v4";
import { ListReducerActionType } from "../types/Reducer/List";
import { authCookieName } from "../types/User";

type IndexProps = {
  initialLists: TodoList[];
  initialItems: TodoItem[];
};

const fetchData = (add = "", cookie = "") => {
  const options = cookie
    ? {
        headers: {
          cookie
        }
      }
    : {};

  return Promise.all([
    fetch(add + "/api/lists", options)
      .then(response => {
        if (!response.ok) return [];
        return response.json();
      })
      .then(data => data as TodoList[]),
    fetch(add + "/api/todos", options)
      .then(response => {
        if (!response.ok) return [];
        return response.json();
      })
      .then(data => data as TodoItem[])
  ]);
};
const Index = ({ initialItems, initialLists }: IndexProps) => {
  const { user } = useContext(userContext);
  const [reloadItems, setReloadItems] = useState(initialItems);
  const [reloadLists, setReloadLists] = useState(initialLists);
  const pusher = useRef<Pusher.Pusher>(null);

  useEffect(() => {
    if (user) {
      fetchData().then(([lists, items]) => {
        setReloadItems(items);
        setReloadLists(lists);
      });
      pusher.current = new Pusher(pusherConfig.key, {
        cluster: pusherConfig.cluster
      });

      return () => {
        pusher.current.disconnect();
      };
    }
  }, [user]);

  const {
    syncConnected,
    lists,
    items,
    clientId,
    dispatchItems,
    dispatchLists
  } = useTodoSubscription(pusher.current, reloadLists, reloadItems);

  const markCompleted = useCallback(
    (item: TodoItem) => {
      item.completed = !item.completed;
      dispatchItems({ type: ListReducerActionType.Update, data: item });

      fetch(`/api/todos/${item.id}`, {
        method: "PUT",
        headers: {
          "x-client-id": clientId,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(item)
      }).then(response => {
        if (!response.ok) {
          item.completed = !item.completed;
          dispatchItems({ type: ListReducerActionType.Update, data: item });
        }
      });
    },
    [clientId]
  );

  const deleteCompleted = useCallback(
    (list: TodoList) => {
      const toDelete = items.filter(
        item => item.listId === list.id && item.completed
      );

      const toDeleteIds = toDelete.map(item => item.id);

      dispatchItems({
        type: ListReducerActionType.Remove,
        data: toDeleteIds
      });

      fetch(`/api/todos?ids=${toDeleteIds.join(",")}`, {
        method: "DELETE",
        headers: {
          "x-client-id": clientId
        }
      }).then(response => {
        if (!response.ok) {
          dispatchItems({ type: ListReducerActionType.Add, data: toDelete });
        }
      });
    },
    [items, clientId]
  );

  const createTodoItem = useCallback(
    async (text: string, listId: string) => {
      const newItem = new TodoItem();
      newItem.text = text;
      newItem.listId = listId;
      newItem.id = uuid();

      dispatchItems({ type: ListReducerActionType.Add, data: newItem });
      return await fetch(`/api/lists/${listId}/todos`, {
        method: "POST",
        headers: {
          "x-client-id": clientId,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(newItem)
      })
        .then(response => {
          if (!response.ok) {
            dispatchItems({
              type: ListReducerActionType.Remove,
              data: newItem.id
            });
            throw Error(response.statusText);
          }
          return response.json();
        })
        .then(body => {
          newItem.id = body.id;
          dispatchItems({
            type: ListReducerActionType.Update,
            data: newItem
          });
        });
    },
    [clientId]
  );

  const createTodoList = useCallback(
    async (title: string) => {
      const newList = new TodoList();
      newList.title = title;
      newList.id = uuid();
      dispatchLists({ type: ListReducerActionType.Add, data: newList });
      return await fetch("/api/lists", {
        method: "POST",
        headers: {
          "x-client-id": clientId,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(newList)
      })
        .then(response => {
          if (!response.ok) {
            dispatchLists({
              type: ListReducerActionType.Remove,
              data: newList.id
            });
            throw Error(response.statusText);
          }
          return response.json();
        })
        .then(body => {
          newList.id = body.id;
          dispatchLists({
            type: ListReducerActionType.Update,
            data: newList
          });
        });
    },
    [clientId]
  );

  const deleteList = useCallback(
    (list: TodoList) => {
      dispatchLists({ type: ListReducerActionType.Remove, data: list.id });
      fetch(`/api/lists/${list.id}`, {
        method: "DELETE",
        headers: {
          "x-client-id": clientId
        }
      }).then(response => {
        if (!response.ok) {
          dispatchLists({ type: ListReducerActionType.Add, data: list });
        }
      });
    },
    [clientId]
  );

  return (
    <div
      style={{
        minWidth: 200,
        maxWidth: 400,
        margin: "50px auto"
      }}
    >
      <h1>Todos</h1>
      <todoContext.Provider
        value={{
          lists,
          items,
          deleteCompleted,
          markCompleted,
          createTodoItem,
          createTodoList,
          deleteList
        }}
      >
        {lists.map(list => (
          <TodoListJSX key={list.id} list={list} />
        ))}
        <NewTodoList />
      </todoContext.Provider>

      <p>Sync: {syncConnected ? "true" : "false"}</p>
    </div>
  );
};

Index.getInitialProps = async ({ req }: NextPageContext) => {
  let add: string;
  let cookie: string;
  if (req) {
    add = process.env.NODE_HOST;
    cookie = "";
    if (req.headers.cookie) {
      const allCookies = req.headers.cookie.split("; ");
      const ourCookie = allCookies.filter(
        cookie => cookie.indexOf(`${authCookieName}=`) === 0
      );
      if (ourCookie.length === 1) cookie = ourCookie[0];
    }
  } else {
    add = "";
  }

  const [initialLists, initialItems] = await fetchData(add, cookie);

  return {
    initialLists,
    initialItems
  } as IndexProps;
};

export default Index;
