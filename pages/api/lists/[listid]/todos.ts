import { NextApiRequest, NextApiResponse } from "next";
import dbConfig from "../../../../config/dbconfig";
import { MongoClient } from "mongodb";
import Pusher from "pusher";
import pusherCOnfig from "../../../../config/pusherconfig";
import { events } from "../../../../types/Pusher/TodoEvents";
import TodoItem from "../../../../types/TodoItem";
import { authCookieName, userSession } from "../../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    body,
    headers: { "x-client-id": socketId },
    method,
    cookies
  } = req;

  const cookie = cookies[authCookieName];
  if (!cookie) return res.status(401).end("no auth fond");
  const { userId } = userSession.parse(cookie);

  const pusher = new Pusher(pusherCOnfig);

  if (method === "POST") {
    const client = await new MongoClient(dbConfig.url).connect();
    const db = client.db(dbConfig.dbName);
    const collection = db.collection("todos");

    body.userId = userId;

    const result = await collection.insertOne(body);
    result.ops[0].id = result.ops[0]._id;
    const newTodoItem = result.ops[0] as TodoItem;

    pusher.trigger(
      ...events.createdItem.trigger({ item: newTodoItem }, socketId as string)
    );

    client.close();
    return res.json(newTodoItem);
  }

  res.status(405).end(`not allowed`);
};

export const config = {
  api: {
    bodyParser: true
  }
};
