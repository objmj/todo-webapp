import { NextApiRequest, NextApiResponse } from "next";
import dbConfig from "../../../../config/dbconfig";
import { MongoClient, ObjectID } from "mongodb";
import Pusher from "pusher";
import pusherCOnfig from "../../../../config/pusherconfig";
import { events } from "../../../../types/Pusher/TodoEvents";
import { authCookieName, userSession } from "../../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { listid },
    headers: { "x-client-id": socketId },
    method,
    cookies
  } = req;

  const listId = listid as string;

  const cookie = cookies[authCookieName];
  if (!cookie) return res.status(401).end("no auth fond");
  const { userId } = userSession.parse(cookie);

  const client = await new MongoClient(dbConfig.url).connect();
  const _id = new ObjectID(listId);
  const pusher = new Pusher(pusherCOnfig);

  await (async () => {
    const db = client.db(dbConfig.dbName);
    const collection = db.collection("lists");

    if (method === "DELETE") {
      const result = await collection.deleteOne({ _id, userId });

      const todosResult = await db
        .collection("todos")
        .deleteMany({ listId, userId });

      pusher.trigger(
        ...events.deletedList.trigger({ listId }, socketId as string)
      );

      return res.json([result.result, todosResult.result]);
    }
    return res.status(405).end("not allowed");
  })();

  client.close();
};
