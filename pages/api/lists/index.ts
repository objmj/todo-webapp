import { NextApiRequest, NextApiResponse } from "next";
import dbConfig from "../../../config/dbconfig";
import { MongoClient, ObjectID } from "mongodb";
import Pusher from "pusher";
import pusherCOnfig from "../../../config/pusherconfig";
import { events } from "../../../types/Pusher/TodoEvents";
import TodoList from "../../../types/TodoList";
import { authCookieName, userSession } from "../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    method,
    headers: { "x-client-id": socketId },
    body,
    cookies
  } = req;

  const cookie = cookies[authCookieName];
  if (!cookie) return res.status(401).end("no auth fond");
  const { userId } = userSession.parse(cookie);
  const client = await new MongoClient(dbConfig.url).connect();
  const pusher = new Pusher(pusherCOnfig);

  await (async () => {
    const db = client.db(dbConfig.dbName);
    const collection = db.collection("lists");

    if (method === "GET") {
      const result = await collection.find({ userId }).toArray();
      return res.json(
        result.map(list => ({
          ...list,
          id: list._id
        }))
      );
    }

    if (method === "POST") {
      body.userId = userId;
      const result = await collection.insertOne(body);
      result.ops[0].id = result.ops[0]._id;
      const newTodoList = result.ops[0] as TodoList;

      pusher.trigger(
        ...events.createdList.trigger({ list: newTodoList }, socketId as string)
      );

      return res.json(newTodoList);
    }
    return res.status(405).end("not allowed");
  })();

  client.close();
};

export const config = {
  api: {
    bodyParser: true
  }
};
