import { NextApiRequest, NextApiResponse } from "next";
import { MongoClient } from "mongodb";
import dbConfig from "../../../config/dbconfig";
import User, {
  userSession,
  authCookieName,
  checkPass,
  SafeUser
} from "../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    method,
    body: { username, password }
  } = req;

  if (method !== "POST") return res.status(405).end("not allowed");

  const client = await new MongoClient(dbConfig.url).connect();
  await (async () => {
    const db = client.db(dbConfig.dbName);
    const collection = db.collection("users");

    const result = await collection.findOne({ username });
    if (!result) return res.status(401).end("information mismatch");
    result.id = result._id;
    const user = result as User;

    if (!checkPass(user, password))
      return res.status(401).end("information mismatch");

    const token = userSession.generate(user);

    const safeUser = new SafeUser();
    safeUser.id = user.id;
    safeUser.username = user.username;

    res.writeHead(200, {
      "Set-Cookie": `${authCookieName}=${token}; path=/;`,
      "Content-Type": "application/json;charset=utf-8"
    });
    res.end(JSON.stringify(safeUser));
  })();
  client.close();
};

export const config = {
  api: {
    bodyParser: true
  }
};
