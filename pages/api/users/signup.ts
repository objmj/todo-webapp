import { NextApiRequest, NextApiResponse } from "next";
import { genSalt, sha512 } from "../../../utils/crypto";
import { MongoClient } from "mongodb";
import dbConfig from "../../../config/dbconfig";
import User, {
  userSession,
  authCookieName,
  SafeUser
} from "../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    method,
    body: { username, password }
  } = req;
  if (method !== "POST") return res.status(405).end("not allowed");

  const client = await new MongoClient(dbConfig.url).connect();

  const db = client.db(dbConfig.dbName);
  const collection = db.collection("users");

  const checkResult = await collection.findOne({ username });
  if (checkResult) return res.status(401).end("information mismatch");

  const newUser = new User();
  newUser.salt = genSalt();
  newUser.hashedPassword = sha512(password, newUser.salt);
  newUser.username = username;

  const result = await collection.insertOne(newUser);
  newUser.id = { ...result.ops[0]._id };

  const token = userSession.generate(newUser);

  const user = new SafeUser();
  user.id = newUser.id;
  user.username = newUser.username;

  res.writeHead(200, {
    "Set-Cookie": `${authCookieName}=${token}; path=/;`,
    "Content-Type": "application/json;charset=utf-8"
  });
  res.end(JSON.stringify(user));
};

export const config = {
  api: {
    bodyParser: true
  }
};
