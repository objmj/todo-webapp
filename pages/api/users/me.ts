import { NextApiRequest, NextApiResponse } from "next";
import dbConfig from "../../../config/dbconfig";
import { MongoClient, ObjectID } from "mongodb";
import { authCookieName, userSession, SafeUser } from "../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { method, body, cookies } = req;

  const cookie = cookies[authCookieName];
  if (!cookie) return res.status(401).end("no auth fond");
  const auth = userSession.parse(cookie);
  const id = new ObjectID(auth.userId);

  const client = await new MongoClient(dbConfig.url).connect();
  await (async () => {
    const db = client.db(dbConfig.dbName);
    const collection = db.collection("users");

    if (method === "GET") {
      const result = await collection.findOne({ _id: id });

      const user = new SafeUser();
      user.username = result.username;
      user.id = result._id;

      return res.json(user);
    }
    if (method === "PUT") {
      const result = await collection.findOneAndUpdate(
        { _id: id },
        { $set: body },
        { returnOriginal: false }
      );
      return res.json(result.value);
    }
  })();

  client.close();
  return res.status(405).end("not allowed");
};

export const config = {
  api: {
    bodyParser: true
  }
};
