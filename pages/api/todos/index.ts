import { NextApiRequest, NextApiResponse } from "next";
import dbConfig from "../../../config/dbconfig";
import { MongoClient, ObjectID } from "mongodb";
import Pusher from "pusher";
import pusherCOnfig from "../../../config/pusherconfig";
import { events } from "../../../types/Pusher/TodoEvents";
import { authCookieName, userSession } from "../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { ids },
    headers: { "x-client-id": socketId },
    method,
    cookies
  } = req;

  const cookie = cookies[authCookieName];
  if (!cookie) return res.status(401).end("no auth fond");
  const { userId } = userSession.parse(cookie);

  const client = await new MongoClient(dbConfig.url).connect();
  const pusher = new Pusher(pusherCOnfig);

  await (async () => {
    const db = client.db(dbConfig.dbName);
    const collection = db.collection("todos");

    if (method === "GET") {
      const result = await collection.find({ userId }).toArray();
      return res.json(
        result.map(item => ({
          ...item,
          id: item._id
        }))
      );
    }
    if (method === "DELETE") {
      const idsToDelete = (ids as string).split(",");

      const objIdsToDelete = idsToDelete.map(s => new ObjectID(s));
      const result = await collection.deleteMany({
        userId,
        _id: { $in: objIdsToDelete }
      });
      pusher.trigger(
        ...events.deletedItems.trigger(
          { deletedIds: idsToDelete },
          socketId as string
        )
      );

      return res.json(result.result);
    }
    return res.status(405).end("not allowed");
  })();

  client.close();
};
