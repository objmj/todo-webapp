import { NextApiRequest, NextApiResponse } from "next";
import dbConfig from "../../../config/dbconfig";
import { MongoClient, ObjectID } from "mongodb";
import Pusher from "pusher";
import pusherCOnfig from "../../../config/pusherconfig";
import { events } from "../../../types/Pusher/TodoEvents";
import TodoItem from "../../../types/TodoItem";
import { userSession, authCookieName } from "../../../types/User";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const {
    query: { todoid },
    body,
    headers: { "x-client-id": socketId },
    method,
    cookies
  } = req;

  const cookie = cookies[authCookieName];
  if (!cookie) return res.status(401).end("no auth fond");
  const { userId } = userSession.parse(cookie);
  const _id = new ObjectID(todoid as string);

  const client = await new MongoClient(dbConfig.url).connect();
  const pusher = new Pusher(pusherCOnfig);

  await (async () => {
    const db = client.db(dbConfig.dbName);
    const collection = db.collection("todos");

    if (method === "GET") {
      const result = await collection.findOne({
        _id,
        userId
      });
      return res.json({
        ...result,
        id: result._id
      });
    }
    if (method === "PUT") {
      delete body._id;
      const result = await collection.findOneAndUpdate(
        { _id, userId },
        { $set: body },
        { returnOriginal: false }
      );
      const updatedItem = result.value as TodoItem;
      pusher.trigger(
        ...events.updatedItem.trigger({ item: updatedItem }, socketId as string)
      );

      return res.json(updatedItem);
    }
    return res.status(405).end("not allowed");
  })();

  client.close();

  res.status(401).end(`not allowed`);
};

export const config = {
  api: {
    bodyParser: true
  }
};
