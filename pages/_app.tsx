import React from "react";
import "isomorphic-unfetch";

import App from "next/app";
import Head from "next/head";
import { userContext } from "../contexts/userContext";
import { bodyStyle, colorTheme, ColorTheme } from "../utils/useStyles";
import Login from "../components/Login";
import { authCookieName, UserTheme, SafeUser } from "../types/User";

type State = {
  theme: ColorTheme;
  loading: boolean;
  online: boolean;
  user: SafeUser;
};

class MyApp extends App {
  state: State = {
    theme: colorTheme(UserTheme.light),
    loading: true,
    online: true,
    user: null
  };

  componentDidMount() {
    window.addEventListener("load", () => {
      const handleNetworkChange = event => {
        this.setState({ online: navigator.onLine });
      };
      window.addEventListener("online", handleNetworkChange);
      window.addEventListener("offline", handleNetworkChange);
    });

    fetch("/api/users/me")
      .then(response => {
        if (!response.ok) throw Error("no auth found");
        return response.json();
      })
      .then(user => {
        this.setState({ user });
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        this.setState({ loading: false });
      });
  }

  updateTheme = (newTheme: UserTheme) => {
    const { theme } = this.state;
    this.setState({ theme: colorTheme(newTheme) });
    fetch("/api/users/me", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ theme: newTheme })
    }).then(response => {
      if (!response.ok) {
        this.setState({ theme });
      }
    });
  };

  attemptLoginOrSignup = (url: "login" | "signup") => (
    username: string,
    password: string
  ) =>
    fetch("/api/users/" + url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ username, password })
    })
      .then(response => {
        if (!response.ok) throw Error("info mismatch");
        return response.json();
      })
      .then(user => {
        this.setState({ user });
      });

  logout = () => {
    document.cookie =
      authCookieName + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    this.setState({ user: null });
  };

  attemptLogin = this.attemptLoginOrSignup("login");
  attemptSignup = this.attemptLoginOrSignup("signup");

  render() {
    const {
      props: { Component, pageProps },
      state: { loading, theme, user, online },
      updateTheme,
      attemptLogin,
      attemptSignup,
      logout
    } = this;
    const { className, styles } = bodyStyle(theme);
    return (
      <div className={className}>
        <Head>
          <title>Todo</title>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <meta charSet="utf-8" />
          <meta name="theme-color" content="#72B340" />
          <meta name="description" content="IT Minds TODO applications" />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/static/icon.png"
          />
          <link rel="preload" href="/static/loading.svg" as="image" />
          <link rel="manifest" href="/static/manifest.json" />
          <link rel="shortcut icon" href="/static/icon.png" />
        </Head>
        <userContext.Provider
          value={{
            theme,
            updateTheme,
            user,
            attemptLogin,
            attemptSignup,
            logout
          }}
        >
          {loading && <img src="/static/loading.svg" />}
          {!loading && !user && <Login />}
          {!loading && user && (
            <>
              <Component {...pageProps} />
              <button onClick={logout}>Logout</button>
              <p>{online ? "online" : "offline"}</p>
            </>
          )}
        </userContext.Provider>
        <style jsx>{styles}</style>
        <style jsx>{`
          img {
            display: flex;
            margin: auto;
          }
        `}</style>
        <style jsx global>{`
          body: {
            margin: 0;
          }
        `}</style>
      </div>
    );
  }
}

export default MyApp;
