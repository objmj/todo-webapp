import TodoItem from "../types/TodoItem";
import { useContext, MouseEvent, useState, useCallback } from "react";
import { todoContext } from "../contexts/todoContext";
import CircularImage from "./CircularImage";

type TodoItemProps = {
  item?: TodoItem;
  grey?: boolean;
  listId?: string;
};

const Item = ({ item }: TodoItemProps) => {
  const { markCompleted: markCompleted } = useContext(todoContext);

  const safeMarkcomplete = (event: MouseEvent) => {
    event.preventDefault();
    markCompleted(item);
  };

  const checkStyle = item.completed
    ? {
        fontStyle: "italic",
        textDecoration: "line-through",
        color: "grey"
      }
    : null;

  return (
    <>
      <CircularImage onClick={safeMarkcomplete}>
        {item.completed && (
          <img style={{ width: "100%" }} src="/static/check.svg" />
        )}
      </CircularImage>
      <p style={{ fontSize: "12pt", margin: 5, ...checkStyle }}>{item.text}</p>
    </>
  );
};

const NewItem = ({ listId }) => {
  const [newText, setNewText] = useState("");
  const { createTodoItem: addTodoItem } = useContext(todoContext);

  const create = useCallback(() => {
    setNewText("");
    addTodoItem(newText, listId).catch(err => {
      setNewText(newText);
    });
  }, [newText]);

  return (
    <>
      <CircularImage border={false} onClick={create}>
        {newText && <img style={{ width: "100%" }} src="/static/add.svg" />}
      </CircularImage>
      <input
        style={{
          background: "unset",
          border: "0px",
          margin: 5,
          fontSize: "12pt"
        }}
        value={newText}
        onChange={e => setNewText(e.target.value)}
        placeholder="..."
      />
    </>
  );
};

export default ({ item, grey, listId }: TodoItemProps) => {
  return (
    <div
      style={{
        background: grey ? "whitesmoke" : "white"
      }}
    >
      <div style={{ padding: 5, display: "flex" }}>
        {item ? <Item item={item} /> : <NewItem listId={listId} />}
      </div>
    </div>
  );
};
