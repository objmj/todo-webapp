import {
  useState,
  ChangeEvent,
  FormEvent,
  useContext,
  MouseEvent,
  useCallback,
  useRef,
  useEffect
} from "react";
import { userContext } from "../contexts/userContext";

type LoginForm = {
  username: string;
  password: string;
};

function useForm<T>() {
  const [values, setValues] = useState<T>({} as T);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    event.persist();
    setValues(values => ({
      ...values,
      [event.target.name]: event.target.value
    }));
  };

  return {
    handleChange,
    values
  };
}

export default () => {
  const { attemptLogin, attemptSignup } = useContext(userContext);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const {
    values: { password, username },
    handleChange
  } = useForm<LoginForm>();

  const login = useCallback(
    (event: FormEvent) => {
      event.preventDefault();
      setLoading(true);
      setError(false);
      attemptLogin(username, password).catch(err => {
        setLoading(false);
        setError(true);
      });
    },
    [password, username]
  );

  const signup = useCallback(
    event => {
      setLoading(true);
      setError(false);
      attemptSignup(username, password).catch(err => {
        setLoading(false);
        setError(true);
      });
    },
    [password, username]
  );

  return (
    <form onSubmit={login}>
      <div>
        <label>Username</label>
        <div>
          <input
            type="text"
            name="username"
            onChange={handleChange}
            value={username}
            required
          />
        </div>
      </div>
      <div>
        <label>Password</label>
        <div>
          <input
            type="password"
            name="password"
            onChange={handleChange}
            value={password}
            required
          />
        </div>
      </div>
      <button disabled={loading} type="submit">
        Login
      </button>
      <button disabled={loading} onClick={signup}>
        Signup
      </button>
      {error && <p>Error</p>}
    </form>
  );
};
