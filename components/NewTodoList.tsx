import { useCallback, useState, useContext } from "react";
import { todoContext } from "../contexts/todoContext";

export default () => {
  const [newText, setNewText] = useState("");
  const { createTodoList: addTodoList } = useContext(todoContext);

  const create = useCallback(() => {
    setNewText("");
    addTodoList(newText).catch(err => {
      setNewText(newText);
    });
  }, [newText]);

  return (
    <div>
      <span
        style={{
          borderRadius: "50%",
          width: 15,
          height: 15,
          cursor: "pointer",
          border: "solid 0px grey",
          margin: 5
        }}
        onClick={create}
      >
        {newText && <img style={{ width: "100%" }} src="/static/add.svg" />}
      </span>
      <input
        style={{
          background: "unset",
          border: "0px",
          margin: 5,
          fontSize: "12pt"
        }}
        value={newText}
        onChange={e => setNewText(e.target.value)}
        placeholder="..."
      />
      <style jsx>{`
        @keyframes bg-scrolling-reverse {
          0% {
            background-position: -50px 0px;
          }
          50% {
            background-position: 0px 0px;
          }
          100% {
            background-position: -50px 0px;
          }
        }

        div {
          display: flex;
          padding: 5px;
          background: repeating-linear-gradient(
            45deg,
            white,
            white 10px,
            whitesmoke 10px,
            whitesmoke 20px
          );
          animation: bg-scrolling-reverse ease 3s infinite;
          border: solid 1px grey;
          margin-top: -1px;
        }
      `}</style>
    </div>
  );
};
