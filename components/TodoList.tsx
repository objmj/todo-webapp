import TodoItem from "./TodoItem";
import TodoItemType from "../types/TodoItem";
import TodoListType from "../types/TodoList";
import ItemListOptions from "./ItemListOptions";
import { useState, useMemo, useContext } from "react";
import { todoContext } from "../contexts/todoContext";
import CircularImage from "./CircularImage";

type Props = {
  list: TodoListType;
};

export default ({ list }: Props) => {
  const [filter, setFilter] = useState<(item: TodoItemType) => boolean>(
    () => item => true
  );
  const [collapsed, setCollapsed] = useState(true);

  const { items, deleteList } = useContext(todoContext);

  const filteredItems = useMemo(
    () => items.filter(item => item.listId === list.id).filter(filter),
    [filter, items]
  );
  const leftCount = useMemo(
    () =>
      items
        .filter(item => item.listId === list.id)
        .filter(item => !item.completed).length,
    [items]
  );

  return (
    <>
      <div
        style={{
          border: "solid 1px grey",
          marginTop: -1,
          background: "white"
        }}
      >
        <div
          style={{
            padding: 5,
            display: "flex"
          }}
        >
          <CircularImage
            border={false}
            onClick={() => setCollapsed(!collapsed)}
          >
            <img src={`/static/${collapsed ? "down" : "up"}.svg`} />
          </CircularImage>
          <p style={{ fontSize: "12pt", margin: 5 }}>{list.title}</p>

          <div style={{ flexGrow: 1 }}>
            <CircularImage
              border={false}
              onClick={e => {
                e.preventDefault();
                deleteList(list);
              }}
              style={{ float: "right" }}
            >
              <img src="/static/remove.svg" />
            </CircularImage>
          </div>
        </div>
        {!collapsed && (
          <>
            <hr />
            {filteredItems.map((item, index) => (
              <TodoItem key={item.id} item={item} grey={index % 2 === 0} />
            ))}
            <TodoItem listId={list.id} grey={filteredItems.length % 2 === 0} />
            <hr />
            <ItemListOptions
              list={list}
              setFilter={setFilter}
              left={leftCount}
            />
          </>
        )}
      </div>
      <style jsx>{`
        img {
          border-radius: 50%;
          width: 15px;
          height: 15px;
          border: solid 0px grey;
        }
        hr {
          margin: 0px;
          border: dashed 1px grey;
        }
      `}</style>
    </>
  );
};
