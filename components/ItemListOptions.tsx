import { useState, useContext } from "react";
import { todoContext } from "../contexts/todoContext";

const FilterButton = ({ text, active, onClick }) => (
  <p
    onClick={onClick}
    style={{
      margin: 1,
      cursor: "pointer",
      padding: 2,
      border: active ? "solid 1px grey" : "",
      borderRadius: 3
    }}
  >
    {text}
  </p>
);

export default ({ list, left, setFilter }) => {
  const [activeButton, setActiveButton] = useState("All");
  const { deleteCompleted } = useContext(todoContext);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        fontSize: "9pt",
        color: "grey",
        padding: 5,
        marginBottom: 2
      }}
    >
      <p
        style={{
          margin: 1
        }}
      >
        {left} items left
      </p>
      <div
        style={{
          display: "flex"
        }}
      >
        <FilterButton
          text="All"
          active={activeButton === "All"}
          onClick={() => {
            setActiveButton("All");
            setFilter(() => item => true);
          }}
        />
        <FilterButton
          text="Active"
          active={activeButton === "Active"}
          onClick={() => {
            setActiveButton("Active");
            setFilter(() => item => !item.completed);
          }}
        />
        <FilterButton
          text="Completed"
          active={activeButton === "Completed"}
          onClick={() => {
            setActiveButton("Completed");
            setFilter(() => item => item.completed);
          }}
        />
      </div>
      <p
        style={{
          margin: 1,
          textAlign: "end",
          cursor: "pointer"
        }}
        onClick={() => deleteCompleted(list)}
      >
        Clear completed
      </p>
    </div>
  );
};
