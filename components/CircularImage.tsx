export default ({ children, onClick, border = true, style = {} }) => (
  <div
    style={{
      borderRadius: "50%",
      width: 15,
      height: 15,
      cursor: "pointer",
      border: border ? "solid 1px grey" : "",
      margin: 5,
      ...style
    }}
    onClick={onClick}
  >
    {children}
  </div>
);
