export default {
  url: process.env.DB_URL || "mongodb://localhost:27017",
  dbName: process.env.DB_NAME || "todoApp"
};
