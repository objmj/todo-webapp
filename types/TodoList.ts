import Entity from "./Entity";

export default class TodoList extends Entity {
  title: string;
  userId: string;
}
