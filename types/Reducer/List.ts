import { Reducer } from "react";

/**
 * Different mutating types to be performed in the reducers dispatch.
 */
export enum ListReducerActionType {
  /** Remove one or many item(s) from state */
  Remove,
  /** Replace one or many item(s) from state if present */
  Update,
  /** Append one or many item(s) to state*/
  Add,
  /** Replace or appen one or many item(s) of state*/
  UpdateOrAdd,
  /** Reset the whole state */
  Reset
}

//The conditional type, specifying the type of data depending on the action type
type ActionDataType<
  T,
  U extends keyof T,
  K
> = K extends ListReducerActionType.Reset
  ? T[]
  : K extends ListReducerActionType.Remove
  ? T[U][] | T[U]
  : T[] | T;

//type of the action to be given by the reducer's dispatch method.
type ListReducerActionInput<
  T,
  U extends keyof T,
  K extends ListReducerActionType
> = {
  /** Type of mutation to perform */
  type: K;
  /** Inputs for the mutating action type */
  data: ActionDataType<T, U, K>;
};

//type union, limiting the inputs based on the values of the reducer's dispatched object
type AllListActions<T, U extends keyof T> =
  | ListReducerActionInput<T, U, ListReducerActionType.Remove>
  | ListReducerActionInput<T, U, ListReducerActionType.Update>
  | ListReducerActionInput<T, U, ListReducerActionType.Add>
  | ListReducerActionInput<T, U, ListReducerActionType.Reset>;

/**
 *
 * @typeparam `T` type of the reducer state
 * @typeparam `U` literal type of property of `T`
 * @param {typeof T} key value of `U`
 * @return {Reducer} React reducer for a stateful list of `T`
 *
 *
 *
 * Can be initiated like this
 * `listReducer<Entity, "id">("id")`
 * Where `Entity` is the type of the list
 * and `"id"` is a property key on the type
 * that is to be used to find index in the list
 */
const listReducer = <T extends unknown, U extends keyof T>(
  key: U
): Reducer<T[], AllListActions<T, U>> =>
  /** TEST*/
  (state: T[], action: AllListActions<T, U>) => {
    switch (action.type) {
      case ListReducerActionType.Add:
        if ((action.data as T[]).push) {
          return [...state, ...(action.data as T[])];
        } else {
          return [...state, action.data as T];
        }
      case ListReducerActionType.Update:
        const replace = (t: T) => {
          const index = state.findIndex(i => i[key] === t[key]);
          state[index] = t;
        };
        if ((action.data as T[]).push) {
          (action.data as T[]).forEach(replace);
        } else {
          replace(action.data as T);
        }
        return [...state];
      case ListReducerActionType.Remove:
        if ((action.data as T[keyof T][]).push) {
          return state.filter(
            t => (action.data as T[keyof T][]).indexOf(t[key]) === -1
          );
        } else {
          return state.filter(t => t[key] !== action.data);
        }
      case ListReducerActionType.Reset:
        return [...(action.data as T[])];

      default:
        throw Error("Unsupported action");
    }
  };

export default listReducer;
