import TodoItem from "../TodoItem";
import TodoList from "../TodoList";

import BUILD_ID from "../../config/build";

export const channel = `todo_${BUILD_ID}`;

type Data = {};

type Trigger<T extends Data> = [string, string, T, string];

type EventType<T> = {
  name: string;
  type: T;
  trigger: (data: T, socketId: string) => Trigger<T>;
};

export interface ItemsDeletedData extends Data {
  deletedIds: string[];
}

export interface ItemUpdateData extends Data {
  item: TodoItem;
}

export interface ItemCreateData extends Data {
  item: TodoItem;
}

export interface ListCreateData extends Data {
  list: TodoList;
}

export interface ListDeleteData extends Data {
  listId: string;
}

type EventsType = {
  deletedItems: EventType<ItemsDeletedData>;
  deletedList: EventType<ListDeleteData>;
  updatedItem: EventType<ItemUpdateData>;
  createdItem: EventType<ItemCreateData>;
  createdList: EventType<ListCreateData>;
};

const eventGen = <T>(name: string) =>
  ({
    name,
    type: <T>{},
    trigger: (data, socketId: string) => [channel, name, data, socketId]
  } as EventType<T>);

export const events: EventsType = {
  deletedItems: eventGen<ItemsDeletedData>("deletedItems"),
  deletedList: eventGen<ListDeleteData>("deletedList"),
  updatedItem: eventGen<ItemUpdateData>("updatedItem"),
  createdItem: eventGen<ItemUpdateData>("createdItem"),
  createdList: eventGen<ListCreateData>("createdList")
};
