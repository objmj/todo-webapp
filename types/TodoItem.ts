import Entity from "./Entity";

export default class TodoItem extends Entity {
  text: string;
  completed?: boolean = false;

  listId: string;
  userId: string;
}
