import Entity from "./Entity";
import uuid from "uuid/v4";
import { encrypt, decrypt, sha512 } from "../utils/crypto";

const check = "52f2b898-7136-4e00-b2d3-8a61e6c313a1";

export default class User extends Entity {
  username: string;
  hashedPassword: string;
  salt: string;
}

export enum UserTheme {
  light,
  dark,
  solarized
}

export class SafeUser {
  id: string;
  username: string;
  settings?: {
    theme: UserTheme;
  };
}

export const userSession = {
  generate: (user: User) => {
    const sessionId = uuid();
    return encrypt(`${user.id}$$${sessionId}$$${check}`);
  },
  parse: (token: string) => {
    const dt = decrypt(token);
    const obj = dt.split("$$");
    if (obj[2] !== check) throw Error("token not valid");
    return {
      userId: obj[0],
      sessionId: obj[1]
    };
  }
};

export const checkPass = (user: User, password: string) =>
  sha512(password, user.salt) === user.hashedPassword;

export const authCookieName = "next-1dk5k6l7v-app";
