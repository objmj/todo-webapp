import { useEffect, useState, useReducer } from "react";
import TodoItem from "../types/TodoItem";
import TodoList from "../types/TodoList";

import { events, channel as listenerChannel } from "../types/Pusher/TodoEvents";
import listReducer, { ListReducerActionType } from "../types/Reducer/List";

export const useTodoSubscription = (
  pusher: Pusher.Pusher,
  initLists: TodoList[],
  initItems: TodoItem[]
) => {
  const [syncConnected, setSyncConnected] = useState(false);
  const [lists, dispatchLists] = useReducer(
    listReducer<TodoList, "id">("id"),
    initLists
  );
  const [items, dispatchItems] = useReducer(
    listReducer<TodoItem, "id">("id"),
    initItems
  );
  const [clientId, setClientId] = useState("");

  useEffect(() => {
    console.log("Resetting Items");
    dispatchItems({
      type: ListReducerActionType.Reset,
      data: initItems
    });
  }, [initItems]);

  useEffect(() => {
    console.log("Resetting Lists");
    dispatchLists({
      type: ListReducerActionType.Reset,
      data: initLists
    });
  }, [initLists]);

  useEffect(() => {
    console.log("Reloading Pusher", !!pusher);
    if (!!pusher) {
      pusher.connection.bind("connected", () => {
        console.log("Pusher Connected", pusher.connection.socket_id);
        setClientId(pusher.connection.socket_id);
      });
      pusher.connection.bind("disconnected", () => {
        console.log("Pusher Connected", pusher.connection.socket_id);
        setSyncConnected(false);
      });
      const channel = pusher.subscribe(listenerChannel);
      channel.bind("pusher:subscription_succeeded", () => {
        setSyncConnected(channel.subscribed);
      });
      channel.bind(
        events.deletedItems.name,
        (data: typeof events.deletedItems.type) => {
          console.log("EVENT RECEIVED!", events.deletedItems.name);
          dispatchItems({
            type: ListReducerActionType.Remove,
            data: data.deletedIds
          });
        }
      );

      channel.bind(
        events.updatedItem.name,
        (data: typeof events.updatedItem.type) => {
          console.log("EVENT RECEIVED!", events.updatedItem.name);
          dispatchItems({
            type: ListReducerActionType.Update,
            data: data.item
          });
        }
      );

      channel.bind(
        events.createdItem.name,
        (data: typeof events.createdItem.type) => {
          console.log("EVENT RECEIVED!", events.createdItem.name);
          dispatchItems({ type: ListReducerActionType.Add, data: data.item });
        }
      );

      channel.bind(
        events.createdList.name,
        (data: typeof events.createdList.type) => {
          console.log("EVENT RECEIVED!", events.createdList.name);
          dispatchLists({ type: ListReducerActionType.Add, data: data.list });
        }
      );

      channel.bind(
        events.deletedList.name,
        (data: typeof events.deletedList.type) => {
          console.log("EVENT RECEIVED!", events.deletedList.name);
          dispatchLists({
            type: ListReducerActionType.Remove,
            data: data.listId
          });
        }
      );
      return () => {
        channel.unbind();
      };
    }
  }, [pusher]);

  return {
    syncConnected,
    lists,
    items,
    clientId,
    dispatchLists,
    dispatchItems
  };
};
