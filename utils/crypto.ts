import crypto from "crypto";
const key = Buffer.alloc(32, "aGVsbG8gd29ybGQ=", "base64");
const iv = Buffer.alloc(16, "aGVsbG8gd29ybGQ=", "base64");

export const genSalt = (length = 16) =>
  crypto
    .randomBytes(Math.ceil(length / 2))
    .toString("hex")
    .slice(0, length);

export const sha512 = (password: string, salt = genSalt()) =>
  crypto
    .createHmac("sha512", salt)
    .update(password)
    .digest("hex");

export const encrypt = (text: string) => {
  let cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return encrypted.toString("hex");
};

export const decrypt = (encrypted: string) => {
  let encryptedText = Buffer.from(encrypted, "hex");
  let decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
};
