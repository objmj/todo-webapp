import css from "styled-jsx/css";
import { UserTheme } from "../types/User";

export type ColorTheme = {
  background: string;
  backgroundOff: string;
  accent: string;
  heightlight: string;
  fontColor: string;
  fontColorAlt: string;
};

export const colorTheme: (theme: UserTheme) => ColorTheme = theme =>
  theme === UserTheme.light
    ? {
        background: "whitesmoke",
        backgroundOff: "lightgrey",
        accent: "",
        heightlight: "",
        fontColor: "",
        fontColorAlt: ""
      }
    : theme === UserTheme.dark
    ? {
        background: "whitesmoke",
        backgroundOff: "lightgrey",
        accent: "",
        heightlight: "",
        fontColor: "",
        fontColorAlt: ""
      }
    : {
        background: "whitesmoke",
        backgroundOff: "lightgrey",
        accent: "",
        heightlight: "",
        fontColor: "",
        fontColorAlt: ""
      };

export const bodyStyle = (colors: ColorTheme) => {
  return css.resolve`
    @import url("https://fonts.googleapis.com/css?family=Chilanka&display=swap");
    * {
      font-family: "Chilanka", cursive;
    }
    div {
      background-color: ${colors.background};
    }
  `;
};
