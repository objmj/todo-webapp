import { createContext } from "react";
import { UserTheme, SafeUser } from "../types/User";
import { ColorTheme } from "../utils/useStyles";

type UserContext = {
  theme: ColorTheme;
  updateTheme: (theme: UserTheme) => void;

  user: SafeUser;
  attemptLogin: (username: string, password: string) => Promise<void>;
  attemptSignup: (username: string, password: string) => Promise<void>;
  logout: () => void;
};

export const userContext = createContext<UserContext>(null);
