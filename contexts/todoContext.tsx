import { createContext } from "react";
import TodoList from "../types/TodoList";
import TodoItem from "../types/TodoItem";


type TodoContext = {
  //Getters
  lists: TodoList[];
  items: TodoItem[];

  //Mutators
  markCompleted: (item: TodoItem) => void;
  deleteCompleted: (list: TodoList) => void;
  deleteList: (list: TodoList) => void;
  createTodoItem: (text: string, listId: string) => Promise<void>;
  createTodoList: (title: string) => Promise<void>;
};

export const todoContext = createContext<TodoContext>(null);
